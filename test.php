<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Demo: Build a store locator using Mapbox.js</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet" />
    <script src="https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.js"></script>
    <link href="https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.css" rel="stylesheet" />
    <style>
        body {
            color: #404040;
            font: 400 15px/22px 'Source Sans Pro', 'Helvetica Neue', Sans-serif;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
        }

        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        h1 {
            font-size: 22px;
            margin: 0;
            font-weight: 400;
        }

        a {
            color: #404040;
            text-decoration: none;
        }

        a:hover {
            color: #101010;
        }

        .sidebar {
            position: absolute;
            width: 33.3333%;
            height: 100%;
            top: 0;
            left: 0;
            overflow: hidden;
            border-right: 1px solid rgba(0, 0, 0, 0.25);
        }

        .pad2 {
            padding: 20px;
        }

        .quiet {
            color: #888;
        }

        .map {
            position: absolute;
            left: 33.3333%;
            width: 66.6666%;
            top: 0;
            bottom: 0;
        }

        .heading {
            background: #fff;
            border-bottom: 1px solid #eee;
            min-height: 60px;
            line-height: 60px;
            padding: 0 10px;
        }

        .listings {
            height: 100%;
            overflow: auto;
            padding-bottom: 60px;
        }

        .listings .item {
            display: block;
            border-bottom: 1px solid #eee;
            padding: 10px;
            text-decoration: none;
        }

        .listings .item:last-child {
            border-bottom: none;
        }

        .listings .item .title {
            display: block;
            color: #00853e;
            font-weight: 700;
        }

        .listings .item .title small {
            font-weight: 400;
        }

        .listings .item.active .title,
        .listings .item .title:hover {
            color: #8cc63f;
        }

        .listings .item.active {
            background-color: #f8f8f8;
        }

        ::-webkit-scrollbar {
            width: 3px;
            height: 3px;
            border-left: 0;
            background: rgba(0, 0, 0, 0.1);
        }

        ::-webkit-scrollbar-track {
            background: none;
        }

        ::-webkit-scrollbar-thumb {
            background: #00853e;
            border-radius: 0;
        }

        .clearfix {
            display: block;
        }

        .clearfix:after {
            content: '.';
            display: block;
            height: 0;
            clear: both;
            visibility: hidden;
        }

        /* Marker tweaks */
        .leaflet-popup-close-button {
            display: none;
        }

        .leaflet-popup-content {
            font: 400 15px/22px 'Source Sans Pro', 'Helvetica Neue', Sans-serif;
            padding: 0;
            width: 200px;
        }

        .leaflet-popup-content-wrapper {
            padding: 0;
        }

        .leaflet-popup-content h3 {
            background: #91c949;
            color: #fff;
            margin: 0;
            display: block;
            padding: 10px;
            border-radius: 3px 3px 0 0;
            font-weight: 700;
            margin-top: -15px;
        }

        .leaflet-popup-content div {
            padding: 10px;
        }

        .leaflet-container .leaflet-marker-icon {
            cursor: pointer;
        }
    </style>
</head>

<body>
    <?php require("koneksi.php"); ?>
    <?php
    $sql = "SELECT * FROM markers";
    $result = $conn->query($sql);
    ?>
    <div class="sidebar">
        <div class="heading">
            <h1>Our locations</h1>
            <a href="index.html" class="main-filled-button">Back</a>
            <a href="route.php" class="main-filled-button">Route</a>
        </div>
        <div id="listings" class="listings"></div>
    </div>
    <div id="map" class="map"></div>
    <script>
        L.mapbox.accessToken = 'pk.eyJ1Ijoicm9iZXR5dXN1ZiIsImEiOiJja240ZWFqenMxZ2xsMndvOHBycjJhMGFnIn0.MmGtL7TNtOf5V2kqe9mLwA';
        var geojson = [{
            'type': 'FeatureCollection',

            'features': [

                <?php
                while ($row = $result->fetch_assoc()) {
                    echo '{
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": [ ' . $row['longitude'] . ',' . $row['latitude'] . ']
                    },
                    "properties": {
                         "nama": "' . $row['nama_cabang'] . '",
                      
                        "alamat": "' . $row['alamat'] . '",
                      
                        "kategori":  "' . $row['kategori_bank'] . '"
                      
                    }
                   },';
                }
                ?>

            ]
        }];
        var map = L.mapbox
            .map('map')
            .setView([-7.9666, 112.6326], 16)
            .addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v10'));

        map.scrollWheelZoom.disable();

        var listings = document.getElementById('listings');
        var locations = L.mapbox.featureLayer().addTo(map);

        locations.setGeoJSON(geojson);

        function setActive(el) {
            var siblings = listings.getElementsByTagName('div');
            for (var i = 0; i < siblings.length; i++) {
                siblings[i].className = siblings[i].className
                    .replace(/active/, '')
                    .replace(/\s\s*$/, '');
            }

            el.className += ' active';
        }

        locations.eachLayer(function(locale) {
            // Shorten locale.feature.properties to just `prop` so we're not
            // writing this long form over and over again.
            var prop = locale.feature.properties;

            // Each marker on the map.
            var popup = '<h3>' + prop.nama + '</h3><div>' + prop.alamat;

            var listing = listings.appendChild(document.createElement('div'));
            listing.className = 'item';

            var link = listing.appendChild(document.createElement('a'));
            link.href = '#';
            link.className = 'title';

            link.innerHTML = prop.nama;
            if (prop.kategori) {
                link.innerHTML +=
                    '<br /><small class="quiet">' + prop.kategori + '</small>';
                popup +=
                    '<br /><small class="quiet">' + prop.kategori + '</small>';
            }

            var details = listing.appendChild(document.createElement('div'));
            details.innerHTML = prop.alamat;
            // if (prop.phone) {
            //     details.innerHTML += ' &middot; ' + prop.phoneFormatted;
            // }

            link.onclick = function() {
                setActive(listing);

                // When a menu item is clicked, animate the map to center
                // its associated locale and open its popup.
                map.setView(locale.getLatLng(), 16);
                locale.openPopup();
                return false;
            };

            // Marker interaction
            locale.on('click', function(e) {
                // 1. center the map on the selected marker.
                map.panTo(locale.getLatLng());

                // 2. Set active the markers associated listing.
                setActive(listing);
            });

            popup += '</div>';
            locale.bindPopup(popup);

            locale.setIcon(
                L.icon({
                    iconUrl: 'https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',
                    iconSize: [29, 36],
                    iconAnchor: [28, 28],
                    popupAnchor: [0, -34]
                })
            );
        });
    </script>
</body>

</html>