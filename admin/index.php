<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Bank Searcher</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="vendor/datatables/dataTables.bootstrap4.min.css">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Mapbox CSS -->
    <script src="https://api.mapbox.com/mapbox.js/v3.3.1/mapbox.js"></script>
    <link rel="stylesheet" href="https://api.mapbox.com/mapbox.js/v3.3.1/mapbox.css">

    <!-- GLJS------------------ -->
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet' />

    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet' />

</head>

<body id="page-top">

    <?php
    require("../koneksi.php");
    ?>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
                <div class="sidebar-brand-text mx-3">Admin <br> Bank Searcher</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Pages</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Other Pages:</h6>
                        <a class="collapse-item" href="404.html">404 Page</a>
                        <a class="collapse-item" href="blank.html">Blank Page</a>
                    </div>
                </div>
            </li>


            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>



        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Search -->
                    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                        <div class="input-group">
                            <div class="input-group-append">
                                <a href="../ourdata.php" class="btn btn-primary" type="button">
                                    <i class="fas fa-globe fa-sm"> | Go To Our Data</i>
                                </a>
                            </div>
                        </div>
                    </form>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="../ourdata.php" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-globe fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                        <!-- Nav Item - Alerts -->




                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Admin</span>
                                <img class="img-profile rounded-circle" src="img/undraw_profile.svg">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Profile
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Settings
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Activity Log
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>

                    </div>

                    <!-- Content Row -->

                    <div class="row">
                        <!-- Total Data Banks Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Total Data Bank Keseluruhan</div>
                                            <?php
                                            $sql1 = "SELECT count(id) as total from markers";
                                            $data = mysqli_query($conn, $sql1);
                                            $new = mysqli_fetch_array($data);
                                            ?>

                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $new['total']; ?> </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Total Data Bank BRI</div>
                                            <?php
                                            $sql1 = "SELECT count(id) as total from markers where kategori_bank = 'BRI'";
                                            $data = mysqli_query($conn, $sql1);
                                            $new = mysqli_fetch_array($data);
                                            ?>

                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $new['total']; ?> </div>
                                        </div>
                                        <div class="col mt-4">
                                            <i> <img src="../assets/images/logo-bank/bri.png" class="float-right" alt="" srcset="" style="width: 45%;"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Total Data Bank BNI</div>
                                            <?php
                                            $sql1 = "SELECT count(id) as total from markers where kategori_bank = 'BNI'";
                                            $data = mysqli_query($conn, $sql1);
                                            $new = mysqli_fetch_array($data);
                                            ?>

                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $new['total']; ?> </div>
                                        </div>
                                        <div class="col">
                                            <i> <img src="../assets/images/logo-bank/bni.png" class="float-right" alt="" srcset="" style="width: 70%;"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Total Data Bank Mandiri</div>
                                            <?php
                                            $sql1 = "SELECT count(id) as total from markers where kategori_bank = 'Mandiri'";
                                            $data = mysqli_query($conn, $sql1);
                                            $new = mysqli_fetch_array($data);
                                            ?>

                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $new['total']; ?> </div>
                                        </div>
                                        <div class="col mt-2">
                                            <i> <img src="../assets/images/logo-bank/mandiri.png" class="float-right" alt="" srcset="" style="width: 80%;"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <!-- Content Row -->

                        <div class="row">

                            <!-- Area Map -->
                            <div class="col-xl-7 col-lg-6">
                                <div class="card shadow mb-12 pb-12">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Maps</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <div class="map-area">
                                            <div id='map' class="map" style='width: 700px; height: 700px;'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Pie Chart -->
                            <div class="col-xl-5 col-lg-6">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Tambah Data Bank</h6>
                                        <div class="dropdown no-arrow">
                                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                                <div class="dropdown-header">Dropdown Header:</div>
                                                <a class="dropdown-item" href="#">Action</a>
                                                <a class="dropdown-item" href="#">Another action</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">Something else here</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <div class="col pt-2 pb-2 px-4">
                                            <form action="model/m_tambah_data.php" method="post">
                                                <div class="form-group">
                                                    <label for="logo_bank"><b> Logo Bank</b></label>
                                                    <input type="text" name="logo_bank" class="form-control" placeholder="Masukkan Logo Bank (ex. Bri.png ,Bni.png)" id="logo_bank">
                                                </div>
                                                <div class="form-group">
                                                    <label for="nama_cabang"><b> Nama Cabang</b></label>
                                                    <input type="text" name="nama_cabang" class="form-control" placeholder="Masukkan Nama Cabang" id="nama_cabang">
                                                </div>
                                                <div class="form-group">
                                                    <label for="alamat"><b> Alamat</b></label>
                                                    <textarea type="text" name="alamat" class="form-control" placeholder="Masukkan Alamat" id="alamat" rows="3"></textarea>
                                                </div>
                                                <div class="row">
                                                    <div class="col form-group">
                                                        <label for="longitude"><b> Longitude</b></label>
                                                        <input type="text" name="longitude" class="form-control" placeholder="Masukkan Longitude" id="longitude">
                                                    </div>
                                                    <div class="col form-group">
                                                        <label for="latitude"><b> Latitude</b></label>
                                                        <input type="text" name="latitude" class="form-control" placeholder="Masukkan Latitude" id="latitude">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="longitude"><b> Kategori Bank</b></label>
                                                    <input type="text" name="kategori_bank" class="form-control" placeholder="Masukkan Kategori Bank" id="kategori_bank">
                                                </div>

                                                <button type="submit" class="btn btn-info px-3 ml-md-3 mb-2 float-right">Tambah
                                                    Data</button>

                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br><br><br>
                        <div class="row">

                            <!-- Content Column -->
                            <div class="col-lg-12 mt-6 mb-4">

                                <!-- DataTales Example -->
                                <div class="card shadow mb-4">
                                    <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary">Data Lokasi Bank Kota Malang</h6>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-8 pt-3">
                                            <!-- Topbar Search -->
                                            <?php

                                            ?>
                                            <form class="d-none d-sm-inline-block form-inline mr-auto  my-2 my-md-0 px-4 navbar-search" action="index.php" method="GET">
                                                <div class="input-group">
                                                    <input type="text" name="kata_cari" class="form-control bg-light border-0 small" value="" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-primary" type="submit">
                                                            <i class="fas fa-search fa-sm"></i>
                                                        </button>

                                                    </div>
                                                    <div class="input-group-append ml-3">
                                                        <a href="index.php" class="btn btn-warning" type="reset">
                                                            <i class="fas fa-sync-alt"></i>
                                                        </a>
                                                    </div>
                                                </div>

                                            </form>

                                        </div>
                                        <div class="col-lg-4">
                                            <a href="export/export_data.php" class="d-none d-sm-inline-block btn btn-sm btn-dark shadow-sm float-right mr-md-3 mt-3"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                                <thead class="text-center">
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Logo Bank</th>
                                                        <th>Nama Cabang</th>
                                                        <th>Alamat</th>
                                                        <th>Longitude</th>
                                                        <th>Latitude</th>
                                                        <th>Kategori Bank</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tfoot class="text-center">
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Logo Bank</th>
                                                        <th>Nama Cabang</th>
                                                        <th>Alamat</th>
                                                        <th>Longitude</th>
                                                        <th>Latitude</th>
                                                        <th>Kategori Bank</th>
                                                        <th>Aksi</th>

                                                    </tr>
                                                </tfoot>
                                                <tbody>
                                                    <tr>

                                                        <?php
                                                        require("../koneksi.php");
                                                        if (isset($_GET['kata_cari'])) {
                                                            $kata_cari = $_GET['kata_cari'];
                                                            //  $query = "SELECT * FROM barang WHERE kode_barang like '%".$kata_cari."%' OR nama_barang like '%".$kata_cari."%' OR jumlah like '%".$kata_cari."%' ORDER BY kode_barang ASC";
                                                            $query = "SELECT * FROM markers WHERE nama_cabang like '%" . $kata_cari . "%' OR alamat like '%" . $kata_cari . "%' OR kategori_bank like '%" . $kata_cari . "%' OR longitude AND latitude like '%" . $kata_cari . "%' ORDER BY id ASC";
                                                        } else {
                                                            $query = "SELECT * FROM markers ORDER BY id ASC";
                                                        }
                                                        $result = mysqli_query($conn, $query);
                                                        if (!$result) {
                                                            $query1 = "Select * from markers";
                                                            $no = 1;
                                                            $sql1 = mysqli_query($conn, $query1);
                                                            while ($row1 = mysqli_fetch_array($sql1)) {
                                                                echo "<tr><th>" . $no++ . "</th>
                                                                <td>" . $row1['logo_bank'] . "</td>
                                                                <td>" . $row1['nama_cabang'] . "</td>
                                                                <td>" . $row1['alamat'] . "</td>
                                                                <td>" . $row1['longitude'] . "</td>
                                                                <td>" . $row1['latitude'] . "</td>
                                                                <td>" . $row1['kategori_bank'] . "</td>
                                                                <td><a href='v_edit_data.php?id_edit=" . $row1['id'] . "' class='btn btn-primary px-4'>Edit</a>
                                                                <a href='model/m_hapus_data.php?id_del=" . $row1['id'] . "' class='btn btn-danger'>Delete</a></td>
                                                                </tr>
                                                                ";
                                                            }
                                                        } else {
                                                            $no = 1;
                                                            while ($row1 = mysqli_fetch_assoc($result)) {
                                                                echo "<tr><th>" . $no++ . "</th>
                                                        <td>" . $row1['logo_bank'] . "</td>
                                                        <td>" . $row1['nama_cabang'] . "</td>
                                                        <td>" . $row1['alamat'] . "</td>
                                                        <td>" . $row1['longitude'] . "</td>
                                                        <td>" . $row1['latitude'] . "</td>
                                                        <td>" . $row1['kategori_bank'] . "</td>
                                                        <td><a href='v_edit_data.php?id_edit=" . $row1['id'] . "' class='btn btn-primary px-4'>Edit</a>
                                                        <a href='model/m_hapus_data.php?id_del=" . $row1['id'] . "' class='btn btn-danger'>Delete</a></td>
                                                        </tr>
                                                        ";
                                                            }
                                                        }

                                                        ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Kelompok2 Website 2021</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Klik "Logout" Jika Ingin Menyudahi Sesi Ini.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="../index.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>


        <?php
        $query3 = 'select * from markers';
        $sql3 = mysqli_query($conn, $query3);
        ?>
        <script>
            L.mapbox.accessToken = 'pk.eyJ1IjoienVsZmFjaHJpZSIsImEiOiJja244b2lzbnAwN216MnBteDdoY3pseDlpIn0.PqiBcspccj60H0nI_ocCZQ';
            var map = L.mapbox.map('map')
                .setView([-7.96, 112.63], 16)
                .addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v11'));
            var myLayer = L.mapbox.featureLayer().addTo(map);



            var geojson = {
                "type": "FeatureCollection",
                "features": [
                    <?php
                    while ($row2 = mysqli_fetch_array($sql3)) {
                        echo '{ "type":"Feature",
                "properties": {
                    "title": "' . $row2['nama_cabang'] . '",
                    "marker-color": "#f86767",
                    "marker-size" : "large",
                    "marker-symbol" : "star"
                },
                "geometry": {
                    "type":"Point",
                    "coordinates" : [' . $row2['longitude'] . ',' . $row2['latitude'] . ']
                }

            },';
                    }
                    ?>
                ]
            }

            myLayer.setGeoJSON(geojson);
            myLayer.on('click', function(e) {
                window.open(e.layer.feature.properties.url);
            });
        </script>


        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>

        <!-- Page level plugins -->
        <script src="vendor/chart.js/Chart.min.js"></script>
        <script src="vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="js/demo/datatables-demo.js"></script>
        <script src="js/demo/chart-area-demo.js"></script>
        <script src="js/demo/chart-pie-demo.js"></script>



</body>

</html>