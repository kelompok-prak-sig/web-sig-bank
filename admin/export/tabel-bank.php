<h3> Data Bank BUMN Kota Malang </h3>

<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead class="text-center">
        <tr>
            <th>No.</th>
            <th>Logo Bank</th>
            <th>Nama Cabang</th>
            <th>Alamat</th>
            <th>Longitude</th>
            <th>Latitude</th>
            <th>Kategori Bank</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tfoot class="text-center">
        <tr>
            <th>No.</th>
            <th>Logo Bank</th>
            <th>Nama Cabang</th>
            <th>Alamat</th>
            <th>Longitude</th>
            <th>Latitude</th>
            <th>Kategori Bank</th>
            <th>Aksi</th>

        </tr>
    </tfoot>
    <tbody>
        <tr>

            <?php
            require("../koneksi.php");
            $query1 = "Select * from markers";
            $no = 1;
            $sql1 = mysqli_query($conn, $query1);
            while ($row1 = mysqli_fetch_array($sql1)) {
                echo "<tr><th>" . $no++ . "</th>
                                                                <td>" . $row1['logo_bank'] . "</td>
                                                                <td>" . $row1['nama_cabang'] . "</td>
                                                                <td>" . $row1['alamat'] . "</td>
                                                                <td>" . $row1['longitude'] . "</td>
                                                                <td>" . $row1['latitude'] . "</td>
                                                                <td>" . $row1['kategori_bank'] . "</td>
                                                                </tr>
                                                                ";
            }

            ?>
    </tbody>
</table>