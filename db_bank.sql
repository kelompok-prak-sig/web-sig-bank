-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2021 at 08:53 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.3.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bank`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `nama`, `password`) VALUES
(1, 'admin', 'Administrator', 'admin'),
(2, 'admin', 'Administrator', 'admin');

-- --------------------------------------------------------
-- Dumping structure for table db_bank.bank
CREATE TABLE IF NOT EXISTS `bank` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama_bank` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table db_bank.bank: ~5 rows (approximately)
/*!40000 ALTER TABLE `bank` DISABLE KEYS */;
INSERT INTO `bank` (`id`, `nama_bank`) VALUES
	(1, 'BRI'),
	(2, 'BRI Center'),
	(3, 'BNI'),
	(4, 'BNI Center'),
	(5, 'Mandiri Center'),
	(6, 'Mandiri');
/*!40000 ALTER TABLE `bank` ENABLE KEYS */;

--
-- Table structure for table `markers`
--

CREATE TABLE `markers` (
  `id` int(11) NOT NULL,
  `logo_bank` varchar(20) NOT NULL,
  `nama_cabang` varchar(50) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `longitude` float(10,7) NOT NULL,
  `latitude` float(10,7) NOT NULL,
  `kategori_bank` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `markers`
--

INSERT INTO `markers` (`id`, `logo_bank`, `nama_cabang`, `alamat`, `longitude`, `latitude`, `kategori_bank`) VALUES
(1, 'bri.png', ' ATM BRI A. Yani Utara Malang', 'Jl. A. Yani, Balearjosari, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6540222, -7.9230819, 'BRI'),
(2, 'bri.png', 'ATM BRI Ajendam', 'Jl. Belakang RSU, Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65111', 112.6329117, -7.9652400, 'BRI'),
(3, 'bri.png', 'ATM BRI Unit DAU', 'Jl. Raya Sengkaling, Sengkaling, Mulyoagung, Kec. Dau, Malang, Jawa Timur 65151', 112.5842209, -7.9079289, 'BRI Center'),
(4, 'bri.png', 'ATM BRI Alfamart', 'Jalan Raya Losari, Sempol, Ardimulyo, Kec. Singosari, Malang, Jawa Timur 65153', 112.6718140, -7.8782892, 'BRI'),
(5, 'bri.png', 'ATM BRI Alfamidi Mertojoyo', 'Alfamidi Mertojoyo, Jl. Mertojoyo, Merjosari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6053162, -7.9297838, 'BRI'),
(6, 'bri.png', 'ATM BRI Alfamart Sawo Jajar', 'Kel, Jl. Raya Sawojajar No.98, Sawojajar, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139', 112.6546097, -7.9740672, 'BRI'),
(7, 'bri.png', 'ATM BRI Unit Dinoyo 2', 'Jl. Tlogomas Ruko Megah Jaya No.10, Tlogomas, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6036148, -7.9335599, 'BRI'),
(8, 'bri.png', 'KCP BRI Tlogomas', 'Ruko BCT, Jl. Tlogomas No.kav 3, Tlogomas, Kec. Lowokwaru, Kota Malang, Jawa Timur 65151', 112.5953445, -7.9213262, 'BRI'),
(9, 'bri.png', 'BRI KCP Sukun', 'Jalan Kebon Sari, Kebonsari, Kecamatan Sukun, Kebonsari, Malang, Kota Malang, Jawa Timur 65149', 112.6189194, -8.0185823, 'BRI'),
(10, 'bri.png', 'ATM BRI Kcp Sawojajar', 'Jl. Danau Toba No.E5, Lesanpuro, Kedungkandang, Malang City, East Java 65139', 112.6592560, -7.9792180, 'BRI Center'),
(11, 'bri.png', 'Bank BRI ATM', 'Jl. Raden Intan, Polowijen, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6511536, -7.9294462, 'BRI'),
(12, 'bri.png', 'ATM BRI Alfamart', 'Jalan Raya Losari, Sempol, Ardimulyo, Kec. Singosari, Malang, Jawa Timur 65153', 112.6725693, -7.8790302, 'BRI'),
(13, 'bri.png', 'ATM BRI Alfamidi Mertojoyo', 'Alfamidi Mertojoyo, Jl. Mertojoyo, Merjosari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6039047, -7.9363270, 'BRI'),
(14, 'bri.png', 'ATM BRI', 'Jl. Trunojoyo, Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65111', 112.6366119, -7.9713159, 'BRI'),
(15, 'bri.png', 'ATM BRI', 'Jl. Besar Ijen, Oro-oro Dowo, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6289825, -7.9642138, 'BRI'),
(16, 'bri.png', 'ATM BRI Indomaret Panjaitan', 'Jl. Mayjend Panjaitan, Penanggungan, Kec. Klojen, Kota Malang, Jawa Timur 65113', 112.6205673, -7.9528232, 'BRI'),
(17, 'bri.png', 'ATM BRI', 'Jl. R. Tumenggung Suryo, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6427689, -7.9535222, 'BRI'),
(18, 'bri.png', 'ATM BRI', 'Jl. Letjen Sutoyo No.105, Lowokwaru, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141', 112.6392136, -7.9427600, 'BRI'),
(19, 'bri.png', 'ATM BRI', 'Jl. Raya Ngijo Karangploso, Kendalsari, Ngijo, Kec. Karang Ploso, Malang, Jawa Timur 65152', 112.6071930, -7.8875170, 'BRI'),
(20, 'bri.png', 'ATM Bank BRI Unit Madyopuro', 'Jl. Raya Madyopuro No.Kav. 3, Madyopuro, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139', 112.6696777, -7.9622798, 'BRI Center'),
(21, 'bri.png', 'ATM BRI', 'Jl. Raya Blimbing Indah, Purwodadi, Malang, Kota Malang, Jawa Timur 65140', 112.6504517, -7.9214759, 'BRI'),
(22, 'bri.png', 'ATM BRI Indomaret', 'Jl. Perusahaan Raya, Karanglo, Banjararum, Kec. Singosari, Malang, Jawa Timur 65153', 112.6500244, -7.9105949, 'BRI'),
(23, 'bri.png', 'ATM BRI', 'Jl. Perusahaan Raya, Tirtsarai, Tanjungtirto, Kec. Singosari, Malang, Jawa Timur 65153', 112.6338882, -7.9020929, 'BRI'),
(24, 'bri.png', 'BRI Unit Dinoyo 2', 'Jl. Tlogomas Ruko Megah Jaya No.10, Tlogomas, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6005859, -7.9269171, 'BRI Center'),
(25, 'bri.png', 'BANK BRI ATM', 'Jl. Raya Kebonagung, Krajan Barat, Kebonagung, Kec. Pakisaji, Kota Malang, Jawa Timur 65162', 112.6180954, -8.0207577, 'BRI'),
(26, 'bri.png', 'ATM BRI', 'Jl. Raya Langsep No.2, Pisang Candi, Kec. Sukun, Kota Malang, Jawa Timur 65143', 112.6126022, -7.9622798, 'BRI'),
(27, 'bri.png', 'ATM BRI', 'Jl. Raya Sekarpuro, Dami, Ampeldento, Kec. Pakis, Kota Malang, Jawa Timur 65154', 112.6812668, -7.9582000, 'BRI'),
(28, 'bri.png', 'ATM BRI', 'SPBU Tlogomas, Jl. Tlogomas, Tlogomas, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6016159, -7.9238572, 'BRI'),
(29, 'bri.png', 'BANK BRI ATM', 'Sengkaling Food Festival, Jl. Raya Sengkaling, Sengkaling, Mulyoagung, Kec. Dau, Malang, Jawa Timur 65151', 112.5851364, -7.9048138, 'BRI'),
(30, 'bri.png', 'ATM BRI Alfamart Ngijo', 'Alfamart Ngijo, Jl. Raya Ngijo Karangploso, Kepuh Utara, Kepuharjo, Kec. Karang Ploso, Kota Malang, Jawa Timur 65153', 112.6208420, -7.9031129, 'BRI'),
(31, 'bri.png', 'ATM BRI', 'Indomaret, Jl. Raya Sumbersari, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6084824, -7.9473190, 'BRI'),
(32, 'bri.png', 'ATM Bank BRI KK UIN Malang', 'Jl. Gajayana No.107, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6089935, -7.9505372, 'BRI'),
(33, 'bri.png', 'ATM BRI', 'Jl. Gajayana, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6088562, -7.9464669, 'BRI'),
(34, 'bri.png', 'ATM BRI', 'Karangbesuki, Kec. Sukun, Kota Malang, Jawa Timur 65149', 112.6064377, -7.9539900, 'BRI'),
(35, 'bri.png', 'ATM BRI', 'Rumah Sakit UMM, Jl. Tlogomas No.45, Dusun Rambaan, Landungsari, Kec. Dau, Malang, Jawa Timur 65144', 112.5987778, -7.9241729, 'BRI'),
(36, 'bri.png', 'BRI Link Dinoyo', 'Jl. Tlogo Suryo Gg.V, Kecamatan Lowokwaru, Tlogomas, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.5993729, -7.9353089, 'BRI'),
(37, 'bri.png', 'ATM BRI', 'Jl. Candi Panggung, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142', 112.6174011, -7.9336090, 'BRI'),
(38, 'bri.png', 'Atm Bri', 'Jl. Kalpataru No.126, Jatimulyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141', 112.6254807, -7.9456062, 'BRI'),
(39, 'bri.png', 'ATM Bank Rakyat Indonesia', 'Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142', 112.6278839, -7.9360852, 'BRI'),
(40, 'bri.png', 'ATM BRI Indomaret Plus', 'Indomaret Plus, Jl. Soekarno Hatta No.22, Jatimulyo, Lowokwaru, Malang City, East Java 65141', 112.6183548, -7.9433112, 'BRI'),
(41, 'bri.png', 'ATM BRI', 'Jl. Puncak Borobudur, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142', 112.6255188, -7.9346080, 'BRI'),
(42, 'bri.png', 'ATM BRI Widya Gama', 'Universitas Widyagama, Jl. Borobudur No.35, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142', 112.6359024, -7.9376678, 'BRI'),
(43, 'bri.png', 'ATM BANK BRI Carefour', 'Jl. Jenderal Ahmad Yani, Blimbing, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6407089, -7.9425139, 'BRI'),
(44, 'bri.png', 'ATM BRI', 'Jl. Laksda Adi Sucipto, Blimbing, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6486053, -7.9417481, 'BRI'),
(45, 'bri.png', 'ATM BRI', 'Jl. Ikan Kakap, Tunjungsekar, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142', 112.6296387, -7.9267869, 'BRI'),
(46, 'bri.png', 'ATM BRI', 'Jl. A. Yani, Purwodadi, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6450272, -7.9348879, 'BRI'),
(47, 'bri.png', 'ATM BRI Teras Pasar Sawojajar', 'JL Danau Bratan Timur, Blok G6 J9, Kedungkandang, Sawojajar, Sawojajar, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139', 112.6570969, -7.9570961, 'BRI'),
(48, 'bri.png', 'BRI ATM', 'Jl. Raya Asrikaton No.8, Boko, Asrikaton, Kec. Pakis, Malang, Jawa Timur 65154', 112.6903992, -7.9492760, 'BRI'),
(49, 'bri.png', 'ATM BRI Wisnuwardhana', 'Madyopuro, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139', 112.6694107, -7.9686141, 'BRI'),
(50, 'bri.png', 'ATM BANK BRI Alfamart', 'Alfamart, Jl. Terusan Danau Sentani, Madyopuro, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139', 112.6659775, -7.9728642, 'BRI'),
(51, 'bri.png', 'ATM BRI SPBU Sulfat - Kota Malang', 'spbu, Jl. Terusan Sulfat, Keduyo, Mangliawan, Kec. Pakis, Malang, Jawa Timur 65139', 112.6611710, -7.9641938, 'BRI'),
(52, 'bri.png', 'ATM BRI', 'Wisma Melati Mayjen Wiyono, Jl. Mayjen.M.Wiyono, Kesatrian, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6486359, -7.9770288, 'BRI'),
(53, 'bri.png', 'ATM BRI BRI Cabang Sukun', 'Jl. S. Supriadi No.78, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148', 112.6210022, -8.0026979, 'BRI'),
(54, 'bri.png', 'ATM BRI PERTOKOAN MITRA (MALANG MARTADINATA)', 'Jl. Agus Salim, Sukoharjo, malang, Kota Malang, Jawa Timur 65146', 112.6326752, -7.9800920, 'BRI'),
(55, 'bri.png', 'ATM BRI', 'Hotel Trio Indah 2, Jl. Brigjend Slamet Riadi, Oro-oro Dowo, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6290665, -7.9681921, 'BRI'),
(56, 'bri.png', 'ATM BRI RS PANTI WALUYA (MALANG MARTADINATA)', 'Jl. Nusakambangan, Kasin, Malang, Kota Malang, Jawa Timur 65100', 112.6244354, -7.9828119, 'BRI'),
(57, 'bri.png', 'ATM Bank BRI', 'Jl. Kapten Piere Tendean, Kasin, Kec. Klojen, Kota Malang, Jawa Timur 65118', 112.6300964, -7.9848518, 'BRI'),
(58, 'bri.png', 'ATM BRI MOG', 'Mal Olympic Garden, Jl. Kawi No.20, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6232300, -7.9729519, 'BRI'),
(59, 'bri.png', 'ATM BRI Rumah Sakit Tentara Dr Soepraden', 'Raumah Sakit Tentara Dr Soepraden, Jl. S. Supriadi, Sukun, Kec. Sukun, Kota Malang, Jawa Timur 65147', 112.6218567, -7.9862118, 'BRI'),
(60, 'bri.png', 'ATM BRI Cabang Pertokoan Sarinah Malang', 'Jl. Jenderal Basuki Rahmat No.31, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6299286, -7.9736319, 'BRI'),
(61, 'bri.png', 'ATM Bank BRI', 'Jl. Jenderal Basuki Rahmat, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6307831, -7.9746518, 'BRI'),
(62, 'bri.png', 'ATM BRI', 'Jl. Belakang RSU, Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65111', 112.6328430, -7.9709120, 'BRI'),
(63, 'bri.png', 'ATM BRI', 'Bank BRI Unit Tanjung Rejo, Jl. I.R. Rais, Tanjungrejo, Kec. Sukun, Kota Malang, Jawa Timur 65119', 112.6148224, -7.9797521, 'BRI'),
(64, 'bri.png', 'ATM BRI Kantor Cabang Pembantu Basuki Rahmat', 'Jl. Jenderal Basuki Rahmat No.22C, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6283798, -7.9749918, 'BRI Center'),
(65, 'bri.png', 'BANK BRI ATM', 'Politeknik Kesehatan Kemenkes Malang, Jl. Besar Ijen No.77 C, Oro-oro Dowo, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6244354, -7.9608788, 'BRI'),
(66, 'bri.png', 'ATM BRI Alfa Midi', 'Jl. S. Supriadi No.35, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65147', 112.6210861, -7.9854441, 'BRI'),
(67, 'bni.png', 'ATM BNI UN MUH MALANG INN', 'Jl. Raya Sengkaling No.1, Jetis, Mulyoagung, Kec. Dau, Malang, Jawa Timur 65151', 112.5932312, -7.9164529, 'BNI'),
(68, 'bni.png', 'ATM BNI DIENG PLAZA', 'Komp Pertokoan Dieng Plaza, Jl. Terusan Raya Dieng, Pisang Candi, Sukun, Pisang Candi, Kec. Sukun, Kota Malang, Jawa Timur 65115', 112.6121750, -7.9699168, 'BNI'),
(69, 'bni.png', 'ATM BNI KLN SAWOJAJAR', 'JL. Danau Bratan Raya C3A16, Kedungkandang, Sawojajar, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139', 112.6582565, -7.9683418, 'BNI'),
(70, 'bni.png', 'ATM BNI', 'Jl. Perusahaan Raya, Losawi, Tanjungtirto, Kec. Singosari, Malang, Jawa Timur 65153', 112.6361084, -7.9081359, 'BNI'),
(71, 'bni.png', 'ATM BNI', 'Jl. Raya Mulyo Agung, Tlogomas, Kec. Lowokwaru, Kota Malang, Jawa Timur 65151', 112.5941162, -7.9193821, 'BNI'),
(72, 'bni.png', 'Atm Bni Dan Atm Bca', 'Jl. Raya Sengkaling No.143, Sengkaling, Mulyoagung, Kec. Dau, Malang, Jawa Timur 65151', 112.5869293, -7.9148312, 'BNI'),
(73, 'bni.png', 'Bank BNI ATM', 'Jl. Soekarno Hatta, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142', 112.6254807, -7.9368620, 'BNI'),
(74, 'bni.png', 'ATM BNI Rumah Sakit UMM', 'Rumah Sakit UMM, Jl. Tlogomas No.45, Dusun Rambaan, Landungsari, Kec. Dau, Malang, Jawa Timur 65144', 112.6000748, -7.9237309, 'BNI'),
(75, 'bni.png', 'ATM BNI POLINEMA', 'Jl. Soekarno Hatta, Jatimulyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141', 112.6172104, -7.9457169, 'BNI'),
(76, 'bni.png', 'ATM BNI PERSADA SWALAYAN MINIMARKET', 'Jl. MT. Haryono, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6112061, -7.9432092, 'BNI'),
(77, 'bni.png', 'ATM BNI KK SOEKARNO HATTA MALANG', 'Ruko SBC, Jl. Soekarno Hatta Kav 2-3, Lowokwaru, Jatimulyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6205978, -7.9422321, 'BNI'),
(78, 'bni.png', 'Bank BNI ATM', 'Ruko Griya Shanta, Jl. Soekarno Hatta, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142', 112.6227493, -7.9399791, 'BNI'),
(79, 'bni.png', 'ATM BNI', 'Jl. Tlogomas, Tlogomas, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.5957413, -7.9201179, 'BNI'),
(80, 'bni.png', 'ATM BNI Taman Sulfat', 'Jl. Taman Sulfat XIII, Bunulrejo, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6497269, -7.9599471, 'BNI'),
(81, 'bni.png', 'ATM BNI', 'Jalan Ruko Vila Bukit Tidar, Merjosari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.5819092, -7.9431882, 'BNI'),
(82, 'bni.png', 'ATM BNI MASJID SABILILLAH', 'Jl. A. Yani, Purwodadi, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6415100, -7.9403381, 'BNI'),
(83, 'bni.png', 'ATM BNI KLN UNIV. MERDEKA NON TUNAI', 'Jl. Terusan Raya Dieng No. 62-64, Pisang Candi, Kec. Sukun, Kota Malang, Jawa Timur 65146', 112.6096954, -7.9722562, 'BNI'),
(84, 'bni.png', 'ATM BNI RK. HARDLENT MEDIKA UTAMA', 'Jl. Galunggung No.56, Gading Kasri, Kec. Klojen, Kota Malang, Jawa Timur 65115', 112.6139832, -7.9678779, 'BNI'),
(85, 'bni.png', 'ATM BNI RS.PANTI NIRMALA', 'Jl. Kolonel Sugiono, Mergosono, Kec. Kedungkandang, Kota Malang, Jawa Timur 65148', 112.6347580, -7.9828382, 'BNI'),
(86, 'bni.png', 'ATM BNI RS.SOEPRAON SUKUN', 'Rumah Sakit Tentara Dr. Soepraden, Jl. S. Supriadi No.22, Sukun, Kec. Sukun, Kota Malang, Jawa Timur 65147', 112.6292648, -8.0015383, 'BNI'),
(87, 'bni.png', 'ATM BNI SPBU TLOGO MAS', 'Jl. Tlogomas No.45, Merjosari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.5890961, -7.9205909, 'BNI'),
(88, 'bni.png', 'ATM BNI SPBU KarangLo', 'Jl. Raya Karanglo, Karanglo, Banjararum, Kec. Singosari, Malang, Jawa Timur 65153', 112.6605072, -7.9134378, 'BNI'),
(89, 'bni.png', 'ATM BNI RS SAIFUL ANWAR', 'Jl. Jaksa Agung Suprapto No.2, Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65111', 112.6388779, -7.9528708, 'BNI'),
(90, 'bni.png', 'ATM BNI MALL OLYMPIC GARDEN', 'Jl. Kawi, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65116', 112.6364746, -7.9817591, 'BNI'),
(91, 'bni.png', 'ATM BNI LINK', 'Jl. Raya Ngijo Karangploso No.292, Kendalsari, Ngijo, Kec. Karang Ploso, Malang, Jawa Timur 65152', 112.6227417, -7.8974199, 'BNI'),
(92, 'bni.png', 'BNI ATM', 'Jl. MT. Haryono, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6071167, -7.9332838, 'BNI'),
(93, 'bni.png', 'ATM BNI SPBU TIDAR', 'Jl. Raya Tidar, Pisang Candi, Kec. Sukun, Surabaya, Jawa Timur 60252', 112.6019516, -7.9463739, 'BNI'),
(94, 'bni.png', 'ATM BNI UM', 'Jl. Semarang No.5, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6156845, -7.9517679, 'BNI'),
(95, 'bni.png', 'ATM BNI KCU MALANG', 'Jl. Basuki Rahmat No. 75-77, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6300964, -7.9584751, 'BNI'),
(96, 'bni.png', 'ATM BNI SPBU KOL. SUGIONO', 'Gading Kasri, Kec. Klojen, Kota Malang, Jawa Timur 65148', 112.6294098, -7.9950562, 'BNI'),
(97, 'bni.png', 'ATM BNI', 'Jl. Cengger Ayam No.25, 02, Tulusrejo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141', 112.6321564, -7.9304070, 'BNI'),
(98, 'bni.png', 'ATM BNI Widya Gama', 'Universitas Widyagama, Jl. Borobudur No.35, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142', 112.6349030, -7.9187989, 'BNI'),
(99, 'bni.png', 'ATM BNI', 'Jalan Ruko Vila Bukit Tidar, Merjosari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.5827179, -7.9235129, 'BNI'),
(100, 'bni.png', 'ATM BNI SPBU TIDAR', 'Jl. Raya Tidar, Pisang Candi, Kec. Sukun, Surabaya, Jawa Timur 60252', 112.5964508, -7.9439149, 'BNI'),
(101, 'bni.png', 'ATM BNI', 'Jl. Gajayana, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6067505, -7.9404221, 'BNI'),
(102, 'bni.png', 'ATM BNI CAFETARIA UB', 'Haryo, Jl. MT. Haryono, Ketawanggede, Lowokwaru, Malang City, East Java 65145', 112.6168900, -7.9526172, 'BNI'),
(103, 'bni.png', 'ATM BNI KLN MALANG TOWN SQUARE', 'Jl. Veteran No.2, Penanggungan, Kec. Klojen, Kota Malang, Jawa Timur 65145', 112.6194153, -7.9558759, 'BNI'),
(104, 'bni.png', 'ATM BNI', 'Jl. Ciliwung, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6409683, -7.9516301, 'BNI'),
(105, 'bni.png', 'ATM BNI', 'Jl. Letjen Sutoyo, Rampal Celaket, Kec. Klojen, Kota Malang, Jawa Timur 65141', 112.6365051, -7.9607682, 'BNI'),
(106, 'bni.png', 'ATM BNI', 'Jl. Merdeka Utara, Kiduldalem, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6314392, -7.9764509, 'BNI'),
(107, 'bni.png', 'ATM Center UNISMA', 'Jl. MT. Haryono No.193, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6066513, -7.9365759, 'BNI Center'),
(108, 'bni.png', 'ATM BNI APOTIK CAMELIA', 'Jl. Danau Bratan, Kauman, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139', 112.6632614, -7.9758172, 'BNI'),
(109, 'bni.png', 'ATM BNI RS PANTI WALUYA SAWAHAN', 'Jl. Yulius Usman, Kasin, Kec. Klojen, Kota Malang, Jawa Timur 65117', 112.6249466, -7.9862542, 'BNI'),
(110, 'bni.png', 'ATM BNI ALFAMART MAYJEN PANJAITAN', 'Blk. M Jl. Panjaitan Dalam No.100, Penanggungan, Kec. Klojen, Kota Malang, Jawa Timur 65113', 112.6224060, -7.9537778, 'BNI'),
(111, 'bni.png', 'ATM BNI ALFAMART TEBO-UTARA', 'Pisang Candi, Kec. Sukun, Kota Malang, Jawa Timur 65146', 112.6057587, -7.9755411, 'BNI'),
(112, 'bni.png', 'ATM BNI ATM RS LAVALETTE', 'Jl. W.R. Supratman, Rampal Celaket, Kec. Klojen, Kota Malang, Jawa Timur 65111', 112.6371765, -7.9645371, 'BNI'),
(113, 'bni.png', 'ATM BNI APOTEK KIMIA FARMA IJEN', 'Jl. Besar Ijen, Oro-oro Dowo, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6255341, -7.9632878, 'BNI'),
(114, 'bni.png', 'ATM BNI KAMPUS ITN BEND SIGURA-GURA', 'Jl. Sigura - Gura No.2, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6126328, -7.9562340, 'BNI'),
(115, 'bni.png', 'ATM BNI DEKSA MEDIKA', 'Jl. Kalpataru, Jatimulyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141', 112.6297913, -7.9493451, 'BNI'),
(116, 'bni.png', 'ATM BNI KLN UNIV. MERDEKA', 'Jl. Terusan Dieng, Pisang Candi, Kec. Sukun, Kota Malang, Jawa Timur 65146', 112.6096725, -7.9720941, 'BNI'),
(117, 'bni.png', 'ATM BNI SPBU', 'SPBU 54.651.69, Jl. Danau Toba, Sawojajar, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139', 112.6548920, -7.9807272, 'BNI'),
(118, 'bni.png', 'ATM BNI STASIUN KERETA API MALANG', 'Rampal Celaket, Kec. Klojen, Kota Malang, Jawa Timur 65111', 112.6368637, -7.9761162, 'BNI'),
(119, 'bni.png', 'ATM Bank Bukopin Mitra Plaza I Malang', 'Jl. Agus Salim, Kiduldalem, Malang, Kota Malang, Jawa Timur 65119', 112.6340179, -7.9829702, 'BNI'),
(120, 'bni.png', 'ATM BNI APOTIK BENGAWAN SOLO', 'Jl. R. Tumenggung Suryo No.36, Bunulrejo, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6429977, -7.9634562, 'BNI'),
(121, 'bni.png', 'BNI UNIV.MUHAMMADYAH', 'Jl. Raya, Tlogomas, Kepanjen, Kota Malang, Jawa Timur 65163', 112.5981445, -7.9204912, 'BNI'),
(122, 'bni.png', 'ATM BNI KLN BLIMBING', 'Kesatrian, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6445389, -7.9280882, 'BNI'),
(123, 'bni.png', 'ATM BNI KLN UNIV. MERDEKA NON TUNAI', 'Jl. Terusan Raya Dieng No. 62-64, Pisang Candi, Kec. Sukun, Kota Malang, Jawa Timur 65146', 112.6128693, -7.9720011, 'BNI'),
(124, 'bni.png', 'BNI Universitas Negeri Malang', 'Jl. Surabaya Dalam, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6185455, -7.9653401, 'BNI'),
(125, 'mandiri.png', 'ATM Bank Mandiri SPBU Danau Toba Sawojajar', 'Jl. Danau Toba No.142, Sawojajar, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139', 112.6548157, -7.9811521, 'Mandiri'),
(126, 'mandiri.png', 'ATM Mandiri', 'Pertokoan Blimbing, JL Borobudur, No. 12 & 35, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65126', 112.6365433, -7.9362998, 'Mandiri'),
(127, 'mandiri.png', 'Mandiri ATM Toko Aneka Baru', 'Jl. Jenderal Basuki Rahmat, Kauman, Malang, Kota Malang, Jawa Timur 65119', 112.6355133, -7.9735322, 'Mandiri'),
(128, 'mandiri.png', 'ATM Mandiri', 'Jl. A. Yani No.125, Purwodadi, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6442719, -7.9339180, 'Mandiri'),
(129, 'mandiri.png', 'ATM Mandiri', 'Alfa Midi, Jl. Ahmad Yani Jl. Jenderal Ahmad Yani No.85, Banurejo, Kepanjen, Kec. Kepanjen, Malang, Jawa Timur 65126', 112.5658264, -1000.0000000, 'Mandiri'),
(130, 'mandiri.png', 'atm mandiri', 'Indomaret Tidar, Jl. Raya Tidar, Karangbesuki, Kec. Sukun, Kota Malang, Jawa Timur 65146', 112.6097641, -7.9482570, 'Mandiri'),
(131, 'mandiri.png', 'ATM Bank Mandiri', 'Indomaret, Jl. Raya Sumbersari, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6056442, -7.9387360, 'Mandiri'),
(132, 'mandiri.png', 'ATM Mandiri Indomaret', 'Jl. Perusahaan Raya, Karanglo, Banjararum, Kec. Singosari, Malang, Jawa Timur 65153', 112.6516495, -7.8958902, 'Mandiri'),
(133, 'mandiri.png', 'ATM Mandiri', 'Jl. Tlogomas No.53, Tlogomas, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.5939636, -7.9012828, 'Mandiri'),
(134, 'mandiri.png', 'ATM Mandiri', 'Malang, Tunggulwulung, Kec. Lowokwaru, Kota Malang, Jawa Timur', 112.6056366, -7.9270792, 'Mandiri'),
(135, 'mandiri.png', 'ATM Bank Mandiri Indomaret', 'Indomaret, Jl. Danau Kerinci Raya, Sawojajar, Kec. Kedungkandang, Kota Malang, Jawa Timur 65154', 112.6605682, -7.9597220, 'Mandiri'),
(136, 'mandiri.png', 'ATM Bank Mandiri', 'Jl. Besar Ijen, Oro-oro Dowo, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6250610, -7.9628420, 'Mandiri'),
(137, 'mandiri.png', 'Mandiri ATM Indomaret Bendungan Sutami ITN', 'Jl. Bendungan Sutami, Sumbersari, Malang, Kota Malang, Jawa Timur 65145', 112.6131821, -7.9590731, 'Mandiri'),
(138, 'mandiri.png', 'Bank Mandiri dan ATM Mandiri Malang Galunggung', 'Jl. Galunggung No.43, Gading Kasri, Kec. Klojen, Kota Malang, Jawa Timur 65115', 112.6155701, -7.9679160, 'Mandiri Center'),
(139, 'mandiri.png', 'Mandiri ATM Malang Town Square', 'Jl. Veteran, Sumbersari, Malang, Kota Malang, Jawa Timur 65114', 112.6199112, -7.9557948, 'Mandiri'),
(140, 'mandiri.png', 'ATM MANDIRI', 'Jl. Raya Sekarpuro, Dami, Ampeldento, Kec. Pakis, Kota Malang, Jawa Timur 65154', 112.6819229, -7.9663310, 'Mandiri'),
(141, 'mandiri.png', 'ATM mandiri', 'Jl. Letjend S. Parman, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6391296, -7.9518938, 'Mandiri'),
(142, 'mandiri.png', 'ATM mandiri', 'Jl. Merdeka Barat No.1, Kiduldalem, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6312408, -7.9826641, 'Mandiri'),
(143, 'mandiri.png', 'atm mandiri', 'Jl. Raya Kebonagung, Sonosari, Kebonagung, Kec. Pakisaji, Kota Malang, Jawa Timur 65162', 112.6172943, -8.0275965, 'Mandiri'),
(144, 'mandiri.png', 'ATM Mandiri ATM Center SPBU 54.65131', 'ATM Center SPBU 54.65131, Jl. Bendungan Sutami, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6138306, -7.9565740, 'Mandiri Center'),
(145, 'mandiri.png', 'ATM mandiri', 'Jl. Tlogomas, Tlogomas, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6021957, -7.9307671, 'Mandiri'),
(146, 'mandiri.png', 'Mandiri ATM KK Malang MT Haryono', 'Jalan Mayjend Jl. MT. Haryono No.131A, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6113739, -7.9404049, 'Mandiri'),
(147, 'mandiri.png', 'ATM Mandiri', 'Indomaret, Jl. Cengkeh, Tulusrejo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141', 112.6249161, -7.9463449, 'Mandiri'),
(148, 'mandiri.png', 'ATM mandiri', 'Indomaret, Jl. Candi Panggung, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142', 112.6206589, -7.9355159, 'Mandiri'),
(149, 'mandiri.png', 'ATM Mandiri', 'Jl. Kawi, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6268234, -7.9774361, 'Mandiri'),
(150, 'mandiri.png', 'ATM Mandiri Indomaret Point', 'Indomaret Point, Jl. MT. Haryono, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6136017, -7.9458232, 'Mandiri'),
(151, 'mandiri.png', 'ATM Mandiri Indomaret Panjaitan', 'Indomaret Panjaitan, Jl. Mayjend Panjaitan, Penanggungan, Kec. Klojen, Kota Malang, Jawa Timur 65113', 112.6207733, -7.9515519, 'Mandiri'),
(152, 'mandiri.png', 'ATM MANDIRI', 'Jl. A. Yani, Balearjosari, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6565247, -7.9214301, 'Mandiri'),
(153, 'mandiri.png', 'ATM Mandiri', 'Jl. Janti Bar. No.20, Sukun, Kec. Sukun, Kota Malang, Jawa Timur 65148', 112.6180267, -7.9865718, 'Mandiri'),
(154, 'mandiri.png', 'ATM Bank Mandiri', 'Kantor Cabang Pembantu Malang Suprapto, JL Jaksa Agung Suprapto, 65, Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65111', 112.6319275, -7.9661722, 'Mandiri'),
(155, 'mandiri.png', 'ATM Mandiri', 'Jl. Puncak Borobudur, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142', 112.6250610, -7.9347200, 'Mandiri'),
(156, 'mandiri.png', 'ATM Mandiri Indomaret', 'Indomaret, Jl. Ikan Piranha Atas, Tunjungsekar, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142', 112.6370773, -7.9272389, 'Mandiri'),
(157, 'mandiri.png', 'Mandiri ATM KCP Malang Gatot Subroto', 'Jl. Gatot Subroto, Jodipan, Malang, Kota Malang, Jawa Timur 65127', 112.6367493, -7.9824548, 'Mandiri'),
(158, 'mandiri.png', 'ATM Mandiri', 'Jl. MT. Haryono, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6138992, -7.9444108, 'Mandiri'),
(159, 'mandiri.png', 'ATM Bank Mandiri', 'Jl. Sigura - Gura, Karangbesuki, Kec. Sukun, Kota Malang, Jawa Timur 65145', 112.6060104, -7.9515491, 'Mandiri'),
(160, 'mandiri.png', 'ATM Bank Mandiri', 'Jl. Raya Ngijo Karangploso, Kedawung, Ngijo, Kec. Karang Ploso, Malang, Jawa Timur 65152', 112.6159821, -7.9040880, 'Mandiri'),
(161, 'mandiri.png', 'ATM Mandiri Indomaret', 'Indomaret Mergan, Jl. Larwo E No.16, Sukun, Malang City, East Java 65147', 112.6173401, -7.9734788, 'Mandiri'),
(162, 'mandiri.png', 'Mandiri ATM Indomaret Tlogomas', 'Jalan Mayjend M.T. Haryono No. 210A, Tlogomas, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.5961685, -7.9186769, 'Mandiri'),
(163, 'mandiri.png', 'ATM Mandiri', 'Jl. Simpang Gajayana, Merjosari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6060104, -7.9403310, 'Mandiri'),
(164, 'mandiri.png', 'ATM Mandiri', 'Jalan Kolonel Sugiono, Mergosono, Kedungkandang, Ciptomulyo, Kec. Sukun, Kota Malang, Jawa Timur 65148', 112.6288376, -7.9875922, 'Mandiri'),
(165, 'mandiri.png', 'ATM Bank Mandiri', 'Jalan Sulfat, Purwantoro, Blimbing, Purwantoro, Malang, Kota Malang, Jawa Timur 65126', 112.6499557, -7.9571590, 'Mandiri'),
(166, 'mandiri.png', 'ATM Mandiri Plaza Araya', 'Plaza Araya, JL Blimbing Indah Megah, Purwodadi, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6485825, -7.9219661, 'Mandiri'),
(167, 'mandiri.png', 'ATM Mandiri', 'Jl. Nusakambangan, Kasin, Kec. Klojen, Kota Malang, Jawa Timur 65117', 112.6242065, -7.9831691, 'Mandiri'),
(168, 'mandiri.png', 'ATM Mandiri Malang Town Square', 'Malang Town Square, JL Veteran, Penanggungan, Kec. Klojen, Kota Malang, Jawa Timur 65113', 112.6182785, -7.9531488, 'Mandiri'),
(169, 'mandiri.png', 'ATM Mandiri Indomaret ITN', 'Jalan Sigura-gura, Sumbersari, Lowokwaru, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6093063, -7.9393830, 'Mandiri'),
(170, 'mandiri.png', 'atm mandiri', 'Jalan Akordeon Selatan, Tunggulwulung, Lowokwaru, Tunggulwulung, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141', 112.6129150, -7.9293518, 'Mandiri'),
(171, 'mandiri.png', 'ATM Bank Mandiri Danau Bratan Sawojajar', 'Jl. Danau Bratan No.13, Lesanpuro, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139', 112.6628571, -7.9721751, 'Mandiri'),
(172, 'mandiri.png', 'ATM Bank Mandiri', 'Indomaret, Jl. Melati No.21, Lowokwaru, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141', 112.6309433, -7.9507742, 'Mandiri'),
(173, 'mandiri.png', 'ATM Bank Mandiri', 'Mal Olympic Garden, Jl. Kawi No.20, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6232986, -7.9675379, 'Mandiri'),
(174, 'mandiri.png', 'ATM Bank Mandiri', 'SPBU 54.65123, Jl. Bandung No.5C, Penanggungan, Kec. Klojen, Kota Malang, Jawa Timur 65113', 112.6223602, -7.9531541, 'Mandiri'),
(175, 'mandiri.png', 'ATM Mandiri Brawijaya', 'Jl. Kampus Universitas Brawijaya Jl. Veteran, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6128235, -7.9491720, 'Mandiri'),
(176, 'mandiri.png', 'ATM Mandiri Komplek Perumahan Sawojajar', 'Jl. Danau Toba, Lesanpuro, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139', 112.6597061, -7.9753881, 'Mandiri'),
(177, 'mandiri.png', 'ATM MANDIRI Indomaret Pattimura', 'Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65111', 112.6329041, -7.9673510, 'Mandiri'),
(178, 'mandiri.png', 'Mandiri ATM SPBU Puncak Mandala Tidar', 'Jl. Puncak Mandala, Pisang Candi, Malang, Kota Malang, Jawa Timur 65146', 112.6028671, -7.9598708, 'Mandiri'),
(179, 'mandiri.png', 'Mandiri ATM Mall Alun Alun Ramayana', 'Jl. Merdeka Timur No.1, Kiduldalem, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6314774, -7.9836860, 'Mandiri'),
(180, 'mandiri.png', 'ATM Mandiri', 'Jl. Merdeka Barat No.1, Kiduldalem, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6291962, -7.9795561, 'Mandiri'),
(181, 'mandiri.png', 'Mandiri Unit Mikro Malang Gatot Subroto', 'Jl. Gatot Subroto No.26, Jodipan, Kec. Blimbing, Kota Malang, Jawa Timur 65118', 112.6362305, -7.9768348, 'Mandiri Center'),
(182, 'mandiri.png', 'Bank Mandiri - KC Malang Wahid Hasyim', 'Jl. Kyai H. Wahid Hasyim No.5-7, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6291046, -7.9828448, 'Mandiri Center'),
(183, 'mandiri.png', 'Bank Mandiri Malang Ahmad Yani', 'Jl. A. Yani No.50 A, RW.02, Blimbing, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6421585, -7.9339361, 'Mandiri Center'),
(184, 'mandiri.png', 'Mandiri Unit Mikro Malang Griya Shanta', 'Ruko Griya Shanta Blok Mp-53, Jalan Soekarno Hatta, Mojolangu, Lowokwaru, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142', 112.6282654, -7.9372611, 'Mandiri Center'),
(185, 'mandiri.png', 'Mandiri ATM KCP Merdeka', 'Jl. Merdeka, Kiduldalem, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6311798, -7.9650812, 'Mandiri Center'),
(186, 'mandiri.png', 'Bank Mandiri KCP Malang MT Haryono', 'Jl. MT. Haryono, Dinoyo, Malang, Kota Malang, Jawa Timur 65141', 112.6090546, -7.9406590, 'Mandiri Center'),
(187, 'mandiri.png', 'Mandiri KCP Malang Sawojajar', 'Jl. Danau Toba, Sawojajar, Malang, Kota Malang, Jawa Timur 65139', 112.6598587, -7.9783731, 'Mandiri Center'),
(188, 'mandiri.png', 'Bank Mandiri Malang Suprapto', 'Jl. Jaksa Agung Suprapto No.65, Samaan, Kec. Klojen, Kota Malang, Jawa Timur 65112', 112.6332703, -7.9658699, 'Mandiri Center'),
(189, 'mandiri.png', 'Bank Mandiri KCP Malang Universitas Brawijaya', 'Jl. Veteran No.16a, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6140366, -7.9531970, 'Mandiri Center'),
(190, 'mandiri.png', 'Bank Mandiri KCP Soekarno Hatta', 'Ruko Soekarno Hatta Indah, Jalan Soekarna Hatta Blok D400 Kav. CDE, Mojolangu, Lowokwaru, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141', 112.6249619, -7.9376721, 'Mandiri Center'),
(191, 'bni.png', 'BNI KCP Universitas Brawijaya Malang', 'Jl. Veteran No.16, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6141434, -7.9381800, 'BNI Center'),
(192, 'bni.png', 'BNI UNIV. MERDEKA', 'Jl. Terusan Dieng No.62-64, Pisang Candi, Kec. Sukun, Kota Malang, Jawa Timur 65146', 112.6086731, -7.9688830, 'BNI Center'),
(193, 'bni.png', 'BNI SAWOJAJAR', 'Jalan Raya Sawojajar, Sawojajar, Kedungkandang, Sawojajar, Malang, Kota Malang, Jawa Timur 65139', 112.6581116, -7.9752069, 'BNI Center'),
(194, 'bni.png', 'BNI SOEKARNO HATTA MALANG', 'Jl. Soekarno Hatta, Jatimulyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141', 112.6205139, -7.9392910, 'BNI Center'),
(195, 'bni.png', 'Bank BNI Blimbing', 'Purwodadi, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6441574, -7.9314008, 'BNI Center'),
(196, 'bni.png', 'BNI UNIV.MUHAMMADYAH', 'Jl. Raya, Tlogomas, Kepanjen, Kota Malang, Jawa Timur 65163', 112.5956268, -7.9168448, 'BNI Center'),
(197, 'bni.png', 'BNI Kantor Wilayah Malang', 'BNI Kantor Wilayah Malang', 112.6291046, -7.9758401, 'BNI Center'),
(198, 'bni.png', 'Bank BNI - Kantor Kas Turen', 'No., Jl. Panglima Sudirman No.124, Turen, Kec. Turen, Malang, Jawa Timur 65175', 112.6907959, -8.1664114, 'BNI Center'),
(199, 'bni.png', 'BNI Singosari', 'Jl. Raya Malang - Gempol No.Kel, Losari, Candirenggo, Kec. Singosari, Malang, Jawa Timur 65153', 112.6673660, -7.8868608, 'BNI Center'),
(200, 'bni.png', 'BNI Pasar Besar', 'Jl. Pasar Besar No.151, Sukoharjo, Kec. Klojen, Kota Malang, Jawa Timur 65118', 112.6352768, -7.9857650, 'BNI Center'),
(201, 'bri.png', 'BRI Kantor Cabang Malang Kawi', 'Jl. Kawi No. 20-22, Kauman, Klojen, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119', 112.6257935, -7.9784770, 'BRI Center'),
(202, 'bri.png', 'KCP BRI TLOGOMAS', 'Ruko BCT, Jl. Tlogomas No.kav 3, Tlogomas, Kec. Lowokwaru, Kota Malang, Jawa Timur 65151', 112.5953445, -7.9206491, 'BRI Center'),
(203, 'bri.png', 'Bank BRI KC Malang Sutoyo', 'Jl. Letjen Sutoyo No.105, Lowokwaru, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141', 112.6380539, -7.9549789, 'BRI Center'),
(204, 'bri.png', 'Bank BRI Kanca Malang Sukarno Hatta', 'Bank BRI,jl.soekarno-hatta Ruko Griya Shanta blok DR 11-12, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142', 112.6229248, -7.9373341, 'BRI Center'),
(205, 'bri.png', 'BRI KCP Rampal', 'JL Sudirman Kav. 10-11, No. 71, Ruko Panglima Sudirman, Tanjungrejo, Kec. Sukun, Kota Malang, Jawa Timur 65147', 112.6379013, -7.9724522, 'BRI Center'),
(206, 'bri.png', 'Bank BRI KK UIN Malang', 'Jl. Gajayana No.107, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144', 112.6086502, -7.9350729, 'BRI Center'),
(207, 'bri.png', 'ATM BRI Galunggung', 'Jl. Galunggung, Gading Kasri, Malang, Kota Malang, Jawa Timur 65134', 112.6135254, -7.9669051, 'BRI Center'),
(208, 'bri.png', 'Bank BRI Unit Pasar Besar', 'Jalan Kapten Piere Tendean Ruko 26C, Kasin, Kec. Klojen, Kota Malang, Jawa Timur 65118', 112.6295242, -7.9862838, 'BRI Center'),
(209, 'bri.png', 'BANK BRI Unit Dampit', 'Jl. Semeru Sel., Dampit, Malang, Jawa Timur 65181', 112.7503357, -8.2119217, 'BRI Center'),
(210, 'bri.png', 'Bank BRI Kantor Kas MOG', 'Mall Olympic Garden Lantai 2, ( samping samsat corner ) Jl. Kawi No. 24, Bareng, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65116', 112.6231232, -7.9759698, 'BRI Center'),
(211, 'bri.png', 'Bank BRI KK LANUD', 'Jl. Komud ABD. Saleh No.88b, Krajan, Asrikaton, Kec. Pakis, Malang, Jawa Timur 65154', 112.6970444, -7.9201622, 'BRI Center'),
(212, 'bri.png', 'Bank BRI Pakis Kembar', 'Jl. Raya Pakis No.3, Krajan, Pakisjajar, Kec. Pakis, Malang, Jawa Timur 65154', 112.7155838, -7.9432850, 'BRI Center'),
(213, 'bri.png', 'Bank Rakyat Indonesia', 'Jl. Martadinata No.28-30, Sukoharjo, Kec. Klojen, Kota Malang, Jawa Timur 65118', 112.6338577, -7.9897680, 'BRI Center'),
(214, 'bri.png', 'Bank Bri Kk Klayatan', 'Jl. S. Supriadi No.40, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148', 112.6132584, -7.9718232, 'BRI Center'),
(215, 'bri.png', 'BANK BRI KK UM', 'Jl. Surabaya No.6, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65115', 112.6145706, -7.9418378, 'BRI Center'),
(216, 'bri.png', 'BRI KK RSU Syaiful Anwar', 'Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65111', 112.6321793, -7.9671988, 'BRI Center'),
(217, 'bri.png', 'Bank BRI Kantor Kas Unmer', 'Jl. Terusan Dieng, Pisang Candi, Kec. Sukun, Kota Malang, Jawa Timur 65146', 112.6096802, -7.9688740, 'BRI Center'),
(218, 'bri.png', 'Bank BRI unit LA Sucipto', 'Jl. Laksda Adi Sucipto No.147, Blimbing, Kec. Blimbing, Kota Malang, Jawa Timur 65126', 112.6472244, -7.9348850, 'BRI Center'),
(219, 'bri.png', 'Bank Bri Kcp Unit Blimbing', 'Jl. Borobudur No.15, Blimbing, Kec. Blimbing, Kota Malang, Jawa Timur 65142', 112.6382980, -7.9314852, 'BRI Center'),
(220, 'bri.png', 'KCP BRI Gajayana', 'Jalan Raya Sumbersari No.95, Sumbersari, Lowokwaru, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6094589, -7.9450860, 'BRI Center'),
(221, 'bri.png', 'BRI cabang merjosari', 'Jl. Mertojoyo Sel. No.10, Merjosari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6043091, -7.9389648, 'BRI Center'),
(222, 'bri.png', 'Bank BRI', 'Jl. Bendungan Sutami, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6131897, -7.9522271, 'BRI Center'),
(223, 'bri.png', 'BANK BRI Mojolangu', 'Jl. Candi Panggung No.66, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142Jl. Candi Panggung No.66, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142', 112.6170120, -7.9280839, 'BRI Center'),
(224, 'bri.png', 'BRI KCP Sukun', 'Jalan Kebon Sari, Kebonsari, Kecamatan Sukun, Kebonsari, Malang, Kota Malang, Jawa Timur 65149', 112.6183853, -8.0113859, 'BRI Center'),
(225, 'bri.png', 'ATM BRI BRI Cabang Sukun', 'Jl. S. Supriadi No.78, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148', 112.6181259, -8.0027590, 'BRI Center'),
(226, 'bri.png', 'BRI KCP Rampal', 'JL Sudirman Kav. 10-11, No. 71, Ruko Panglima Sudirman, Tanjungrejo, Kec. Sukun, Kota Malang, Jawa Timur 65147', 112.6378250, -7.9694400, 'BRI Center'),
(227, 'bri.png', 'Bank Bri Kcp Unit Jabung', 'Jl. Letjen Sutoyo No.55, Lowokwaru, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141', 112.6361923, -7.9603019, 'BRI Center'),
(228, 'bri.png', 'Bank Bri Unit Tawangmangu', 'Jl. Sarangan No.21, Lowokwaru, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141', 112.6327209, -7.9590702, 'BRI Center'),
(229, 'bri.png', 'KCP BRI Brawijaya', 'Jl. MT. Haryono, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145', 112.6134949, -7.9438109, 'BRI Center'),
(230, 'bri.png', 'Bank BRI Unit Gondanglegi', 'Jl. Hayam Wuruk, Krajan, Gondanglegi Wetan, Kec. Gondanglegi, Malang, Jawa Timur 65174', 112.6380157, -8.1734715, 'BRI Center');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `markers`
--
ALTER TABLE `markers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `markers`
--
ALTER TABLE `markers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
